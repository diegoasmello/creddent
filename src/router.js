import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/consultas',
      name: 'consults',
      component: () => import('./views/Consults.vue')
    },
    {
      path: '/consultas/:id',
      name: 'consultsdetails',
      component: () => import('./views/ConsultsDetails.vue')
    },
    {
      path: '/pagamento',
      name: 'finances',
      component: () => import('./views/Finances.vue')
    },
    {
      path: '/pagamento/:id',
      name: 'financesdetails',
      component: () => import('./views/FinancesDetails.vue')
    },
    {
      path: '/tratamento',
      name: 'treatment',
      component: () => import('./views/Treatment.vue')
    },
    {
      path: '/tratamento/:id',
      name: 'treatmentdetails',
      component: () => import('./views/TreatmentDetails.vue')
    }
  ]
})
